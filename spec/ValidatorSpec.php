<?php

namespace spec\Paneric\Validation;

use Paneric\Validation\Service\ErrorService;
use Paneric\Validation\Service\MessageService;
use Paneric\Validation\Validator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ValidatorSpec extends ObjectBehavior
{
    public function let(ErrorService $errorService, MessageService $messageService): void
    {
        $alertsCluster = [];

        $this->beConstructedWith($errorService, $messageService, $alertsCluster, Argument::type('string'));
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Validator::class);
    }

    public function it_sets_validation_messages(ErrorService $errorService, MessageService $messageService): void
    {
        $errorService->setErrors(
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('array')
        )->willReturn([]);

        $messageService->setMessages(
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('string')
        )->willReturn([]);

        $this->setMessages([], [], [])->shouldReturn([]);
    }
}
