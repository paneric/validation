<?php

namespace spec\Paneric\Validation\Service;

use Paneric\Validation\Service\ErrorService;
use Paneric\Validation\Service\RuleService;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ErrorServiceSpec extends ObjectBehavior
{
    public function let(RuleService $ruleService): void
    {
        $this->beConstructedWith($ruleService);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(ErrorService::class);
    }

    public function it_sets_errors(RuleService $ruleService): void
    {
        $config = [
            'user' => [
                'has_min_length' => [3],
                'required' => [],
                'is_one_of' => ['one', 'two', 'three'],
                'is_valid_timestamp' => ['Y-m-d H:i:s'],
            ],
        ];

        $errors = [
            'user' => [
                'has_min_length' => [
                    'error' => 'invalid',
                    'arguments' => [3],
                ],
                'required' => [
                    'error' => 'invalid',
                    'arguments' => [],
                ],
                'is_one_of' => [
                    'error' => 'invalid',
                    'arguments' => ['one', 'two', 'three'],
                ],
                'is_valid_timestamp' => [
                    'error' => 'invalid',
                    'arguments' => ['Y-m-d H:i:s'],
                ],
            ],
        ];

        $values = [
            'user' => '',
        ];

        $ruleService->setValues($values)->shouldBeCalled();
        $ruleService->setRequestAttribute(Argument::type('array'))->shouldNOtBeCalled();

        $ruleService->hasMinLength(
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('array')
        )->willReturn('invalid');

        $ruleService->required(
            Argument::type('string'),
            Argument::type('string')
        )->willReturn('invalid');

        $ruleService->isOneOf(
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('array')
        )->willReturn('invalid');

        $ruleService->isValidTimestamp(
            Argument::type('string'),
            Argument::type('string'),
            Argument::type('array')
        )->willReturn('invalid');

        $this->setErrors($config, $values)->shouldReturn($errors);
    }
}
