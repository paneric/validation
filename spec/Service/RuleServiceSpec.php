<?php

namespace spec\Paneric\Validation\Service;

use Paneric\Validation\Service\RuleService;
use PhpSpec\ObjectBehavior;

class RuleServiceSpec extends ObjectBehavior
{
    public function it_is_initializable(): void
    {
        $this->shouldHaveType(RuleService::class);
    }

    public function it_requires(): void
    {
        $this->required('user', 'User')->shouldReturn(null);
        $this->required('user', '')->shouldReturn('invalid');
    }

    public function it_has_min_length(): void
    {
        $this->hasMinLength('user', 'User', [4])->shouldReturn(null);
        $this->hasMinLength('user', 'User', [6])->shouldReturn('invalid');
    }

    public function it_has_max_length(): void
    {
        $this->hasMaxLength('user', 'User', [4])->shouldReturn(null);
        $this->hasMaxLength('user', 'User', [3])->shouldReturn('invalid');
    }

    public function it_has_exact_length(): void
    {
        $this->hasExactLength('user', 'User', [4])->shouldReturn(null);
        $this->hasExactLength('user', 'Users', [4])->shouldReturn('invalid');
    }

    public function it_is_valid_gsm(): void
    {
        $this->isValidGsm('gsm', '+32477950778')->shouldReturn(null);
        $this->isValidGsm('gsm', '+32 495 950 778')->shouldReturn(null);
        $this->isValidGsm('gsm', ' 0032 495 950 778')->shouldReturn(null);
        $this->isValidGsm('gsm', ' 0 484 495 778')->shouldReturn(null);
        $this->isValidGsm('gsm', ' 0032 495 950 778')->shouldReturn(null);
        $this->isValidGsm('gsm', ' 0 484 495 77')->shouldReturn('invalid');
        $this->isValidGsm('gsm', ' 0 684 495 778')->shouldReturn('invalid');
    }

    public function it_is_valid_password(): void
    {
        $this->isValidPassword('password', 'Ab_1jdkrirvnhyeid')->shouldReturn(null);
        $this->isValidPassword('password', 'Ab1jdkrirvnhyeid')->shouldReturn('invalid');
        $this->isValidPassword('password', 'Ab_jdkrirvnhyeid')->shouldReturn('invalid');
        $this->isValidPassword('password', 'Ab_1')->shouldReturn('invalid');
    }

    public function it_is_valid_email(): void
    {
        $this->isValidEmail('email', 'dkdi@dddd.be')->shouldReturn(null);
        $this->isValidEmail('email', '@dddd.be')->shouldReturn('invalid');
        $this->isValidEmail('email', 'dddd.be')->shouldReturn('invalid');
        $this->isValidEmail('email', 'dkdi@dddd')->shouldReturn('invalid');
    }

    public function it_is_one_of(): void
    {
        $this->isOneOf('option', 'a', ['a', 'b', 'c'])->shouldReturn(null);
        $this->isOneOf('option', 'd', ['a', 'b', 'c'])->shouldReturn('invalid');
    }

    public function it_is_same_as(): void
    {
        $this->setValues(['option1' => 'value']);
        $this->isSameAs('option', 'value', ['option1'])->shouldReturn(null);

        $this->setValues(['option1' => 'value1']);
        $this->isSameAs('option', 'value', ['option1'])->shouldReturn('invalid');
    }

    public function it_is_all_alpha(): void
    {
        $this->isAllAlpha('option', 'a')->shouldReturn(null);
        $this->isAllAlpha('option', 'd1')->shouldReturn('invalid');
        $this->isAllAlpha('option', '_-')->shouldReturn(null);
    }

    public function it_is_all_alpha_numeric(): void
    {
        $this->isAllAlphaNumeric('option', 'a')->shouldReturn(null);
        $this->isAllAlphaNumeric('option', '1')->shouldReturn(null);
        $this->isAllAlphaNumeric('option', 'a1')->shouldReturn(null);
        $this->isAllAlphaNumeric('option', 'a1_-')->shouldReturn(null);
        $this->isAllAlphaNumeric('option', 'a1<')->shouldReturn('invalid');
    }

    public function it_is_text(): void
    {
        $this->isText('option', '1._-:;(),*')->shouldReturn(null);
        $this->isText('option', '._-:;(),*<')->shouldReturn('invalid');

    }

    public function it_is_all_numeric(): void
    {
        $this->isAllNumeric('number', '99685')->shouldReturn(null);
        $this->isAllNumeric('number', 'a')->shouldReturn('invalid');
        $this->isAllNumeric('number', 'a584')->shouldReturn('invalid');
    }

    public function it_is_integer(): void
    {
        $this->isInteger('number', '16')->shouldReturn(null);
        $this->isInteger('number', 'a')->shouldReturn('invalid');
        $this->isInteger('number', '1.56')->shouldReturn('invalid');
        $this->isInteger('number', '1,56')->shouldReturn('invalid');
        $this->isInteger('number', 'a.56')->shouldReturn('invalid');
        $this->isInteger('number', '1.a6')->shouldReturn('invalid');
        $this->isInteger('number', '1a6')->shouldReturn('invalid');
    }

    public function it_is_ids_string(): void
    {
        $this->isIdsString('number', ',1,')->shouldReturn(null);
        $this->isIdsString('number', ',a,')->shouldReturn(null);
        $this->isIdsString('number', ',a1,')->shouldReturn(null);
        $this->isIdsString('number', ',1')->shouldReturn('invalid');
        $this->isIdsString('number', '1,')->shouldReturn('invalid');
        $this->isIdsString('number', 'a,')->shouldReturn('invalid');
        $this->isIdsString('number', 'a1,')->shouldReturn('invalid');
        $this->isIdsString('number', ',a')->shouldReturn('invalid');
        $this->isIdsString('number', ',a1')->shouldReturn('invalid');
    }

    public function it_is_float(): void
    {
        $this->isFloat('number', '1.56')->shouldReturn(null);
        $this->isFloat('number', '1,56')->shouldReturn(null);
        $this->isFloat('number', 'a.56')->shouldReturn('invalid');
        $this->isFloat('number', '1.a6')->shouldReturn('invalid');
        $this->isFloat('number', '1,a6')->shouldReturn('invalid');
    }

    public function it_is_valid_timestamp(): void
    {
        $this->isValidTimestamp('time_stamp', '2020/02/14 14:30:15', ['Y/m/d H:i:s'])
            ->shouldReturn(null);
        $this->isValidTimestamp('time_stamp', '2020-02-14 14:30:15', ['Y-m-d H:i:s'])
            ->shouldReturn(null);
        $this->isValidTimestamp('time_stamp', '2020-60-14 14:30:15', ['Y-m-d H:i:s'])
            ->shouldReturn('invalid');
        $this->isValidTimestamp('time_stamp', '2020-02-60 14:30:15', ['Y-m-d H:i:s'])
            ->shouldReturn('invalid');
        $this->isValidTimestamp('time_stamp', '2020-02-14 60:30:15', ['Y-m-d H:i:s'])
            ->shouldReturn('invalid');
        $this->isValidTimestamp('time_stamp', '2020-02-14 14:60:15', ['Y-m-d H:i:s'])
            ->shouldReturn('invalid');
        $this->isValidTimestamp('time_stamp', '2020-02-14 14:30:60', ['Y-m-d H:i:s'])
            ->shouldReturn('invalid');

        $this->isValidTimestamp('time_stamp', '14:30:15 2020-02-14', ['Y-m-d H:i:s'])
            ->shouldReturn('invalid');
        $this->isValidTimestamp('time_stamp', '2020/02/14 14:30:60', ['Y-m-d H:i:s'])
            ->shouldReturn('invalid');
    }

    public function it_is_valid_time_format(): void
    {
        $this->isValidTimeFormat('time', '14:30')->shouldReturn(null);
        $this->isValidTimeFormat('time', '25:30')->shouldReturn('invalid');
        $this->isValidTimeFormat('time', '15:60')->shouldReturn('invalid');
        $this->isValidTimeFormat('time', '15:30:21')->shouldReturn('invalid');
    }

    public function it_image_file_path(): void
    {
        $this->isImageFilePath('file', 'file.gif')->shouldReturn(null);
        $this->isImageFilePath('file', 'folder/file.jpg')->shouldReturn(null);
        $this->isImageFilePath('file', 'folder\file.jpeg')->shouldReturn(null);
        $this->isImageFilePath('file', 'c:\folder/file.png')->shouldReturn(null);
        $this->isImageFilePath('file', 'c:\folder/file.doc')->shouldReturn('invalid');
    }

    public function it_is_url(): void
    {
        $this->isUrl('url', 'http://google.com')->shouldReturn(null);
        $this->isUrl('url', 'https://google.com')->shouldReturn(null);
        $this->isUrl('url', 'http://google.com/search/1')->shouldReturn(null);
        $this->isUrl('url', 'google.com')->shouldReturn(null);
        $this->isUrl('url', 'http://google')->shouldReturn('invalid');
    }

    public function it_is_not_html(): void
    {
        $this->isNotHtml('content', 'content')->shouldReturn(null);
        $this->isNotHtml('content', '<content')->shouldReturn(null);
        $this->isNotHtml('content', 'content>')->shouldReturn(null);
        $this->isNotHtml('content', '<content>')->shouldReturn('invalid');
    }
}
