<?php

namespace spec\Paneric\Validation\Service;

use Paneric\Validation\Service\MessageService;
use PhpSpec\ObjectBehavior;

class MessageServiceSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(MessageService::class);
    }

    public function it_sets_messages(): void
    {
        $config = [
            'user' => [
                'has_min_length' => [3],
                'required' => [],
                'is_one_of' => ['one', 'two', 'three'],
                'is_valid_timestamp' => ['Y-m-d H:i:s'],
            ],
        ];

        $values = [
            'user' => '',
        ];

        $errors = [
            'user' => [
                'has_min_length' => [
                    'error' => 'invalid',
                    'arguments' => [3],
                ],
                'required' => [
                    'error' => 'invalid',
                    'arguments' => [],
                ],
                'is_one_of' => [
                    'error' => 'invalid',
                    'arguments' => ['one', 'two', 'three'],
                ],
                'is_valid_timestamp' => [
                    'error' => 'invalid',
                    'arguments' => ['Y-m-d H:i:s'],
                ],
            ],
        ];

        $alertsCluster = require dirname(__DIR__, 2) . '/src/alerts.php';

        $messages = [
            'user' => [
                'has_min_length' => 'Min length of 3 characters is required.',
                'required' => 'This value is required.',
                'is_one_of' => 'This is not one of expected (one, two, three) values.',
                'is_valid_timestamp' => 'This is not a valid timestamp (Y-m-d H:i:s) value.',
            ],
        ];

        $this->setMessages($config, $values, $errors, $alertsCluster, 'en')->shouldReturn($messages);
    }
}
