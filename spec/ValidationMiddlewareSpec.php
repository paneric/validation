<?php

namespace spec\Paneric\Validation;

use Paneric\Validation\TestDTO;
use Paneric\Validation\ValidationMiddleware;
use Paneric\Validation\Validator;
use PhpSpec\ObjectBehavior;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Prophecy\Argument;

class ValidationMiddlewareSpec extends ObjectBehavior
{
    public function it_is_initializable(Validator $validator): void
    {
        $this->beConstructedWith($validator,[]);

        $this->shouldHaveType(ValidationMiddleware::class);
    }

    public function it_process_1(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
        ResponseInterface $response,
        Validator $validator
    ): void {
        $configs = [
            'route' => [
                'methods' => ['POST'],
                TestDTO::class => [
                    'request_attribute' => 'request.attribute',
                    'rules' => [],
                ],
            ],
        ];

        $this->beConstructedWith($validator, $configs);

        $request->getAttribute('route_name')->willReturn('route');

        $request->getMethod()->willReturn('GET');

        $request->withAttribute('validation', [])->willReturn($request);

        $handler->handle($request)->willReturn($response);

        $this->process($request, $handler)->shouldReturn($response);
    }

    public function it_process_2(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
        ResponseInterface $response,
        Validator $validator
    ): void {
        $configs = [
            'route' => [
                'methods' => ['POST'],
                TestDTO::class => [
                    'rules' => [],
                ],
            ],
        ];

        $this->beConstructedWith($validator, $configs);

        $request->getAttribute('route_name')->willReturn('route');

        $request->getMethod()->willReturn('POST');

        $request->getParsedBody()->willReturn([]);

        $request->getAttribute('authentication')->shouldNotBeCalled();

        $validator->setMessages(
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('array')
        )->willReturn([]);

        $validator->setReport(Argument::type('array'))->shouldBeCalled();

        $request->withAttribute('validation', [TestDTO::class => []])->willReturn($request);

        $handler->handle($request)->willReturn($response);

        $this->process($request, $handler)->shouldReturn($response);
    }

    public function it_process_3(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler,
        ResponseInterface $response,
        Validator $validator
    ): void {
        $configs = [
            'route' => [
                'methods' => ['POST'],
                TestDTO::class => [
                    'request_attribute' => 'authentication',
                    'rules' => [],
                ],
            ],
        ];

        $this->beConstructedWith($validator, $configs);

        $request->getAttribute('route_name')->willReturn('route');

        $request->getMethod()->willReturn('POST');

        $request->getParsedBody()->willReturn([]);

        $request->getAttribute('authentication')->willReturn(['id' => 'id_row']);

        $validator->setMessages(
            Argument::type('array'),
            Argument::type('array'),
            Argument::type('array')
        )->willReturn([]);

        $validator->setReport(Argument::type('array'))->shouldBeCalled();

        $request->withAttribute('validation', [TestDTO::class => []])->willReturn($request);

        $handler->handle($request)->willReturn($response);

        $this->process($request, $handler)->shouldReturn($response);
    }
}
