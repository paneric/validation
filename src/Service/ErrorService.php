<?php

declare(strict_types=1);

namespace Paneric\Validation\Service;

use function is_array;

class ErrorService
{
    private $ruleService;

    public function __construct(RuleService $ruleService)
    {
        $this->ruleService = $ruleService;
    }

    public function setErrors(array $config, array $values, array $requestAttribute = []): array
    {
        $this->ruleService->setValues($values);

        if (!empty($requestAttribute)) {
            $this->ruleService->setRequestAttribute($requestAttribute);
        }

        $errors = [];

        foreach ($config as $field => $rules) {
            $value = $values[$field] ?? '';

            if (!is_array($value)) {
                $errors[$field] = $this->setFieldErrors($field, (string) $value, $rules);
            }

            if (is_array($value)) {
                foreach ($value as $key => $val) {
                    $errors[$field][$key] = $this->setFieldErrors($field, $val, $rules);
                }
            }
        }

        return $errors;
    }

    private function setFieldErrors(string $field, string $value, array $rules): array
    {
        $fieldErrors = [];

        foreach ($rules as $rule => $args) {
            $fieldRuleError = $this->setFieldRuleError($field, $value, $rule, $args);

            $fieldErrors[$rule]['error'] = $fieldRuleError;
            $fieldErrors[$rule]['arguments'] = $args;
        }

        return $fieldErrors;
    }

    private function setFieldRuleError(string $field, string $value, string $rule, array $args): ?string
    {
        $rule = $this->setRule($rule);

        if (!empty($args)) {
            return $this->ruleService->{$rule}($field, $value, $args);
        }

        return $this->ruleService->{$rule}($field, $value);
    }

    private function setRule(string $rule): string
    {
        if (strpos($rule, '_')) {
            $rule = explode('_', $rule);

            foreach ($rule as $key => $value) {
                if ($key > 0) {
                    $rule[$key] = ucfirst($rule[$key]);
                }
            }

            $rule = $this->concatRule($rule);
        }

        return $rule;
    }

    private function concatRule(array $rule): string
    {
        $result = $rule[0];

        unset($rule[0]);

        foreach ($rule as $part) {
            $result .= ucfirst($part);
        }

        return $result;
    }
}
