<?php

declare(strict_types=1);

namespace Paneric\Validation\Service;

use DateTimeImmutable;
use JsonException;

class RuleService
{
    public const STATUS = 'invalid';

    private $requiredValues = [];
    private $masterValues = [];
    private $values;
    private $requestAttribute = [];

    private $repository;


    public function setValues(array $values): void
    {
        $this->values = $values;
    }

    public function setRequestAttribute(array $requestAttribute): void
    {
        $this->requestAttribute = $requestAttribute;
    }


    public function required(string $field, string $value, array $args = null): ?string
    {
        if ($args !== null) {
            if ($args[0] === 'master' && ($value !== '' || $value || $value === '0')) {//if master and exists
                $this->masterValues[$field] = true;
                return null;
            }

            if ($args[0] === 'master' && (($value === '' || !$value) && $value !== '0')) {//if master but does not exist
                $this->masterValues[$field] = false;
                return null;
            }

            if ($args[0] !== 'master' && $this->masterValues[$args[0]]) {//if not master but master exists
                $this->requiredValues[$field] = true;
                return (($value === '' || !$value) && $value !== '0') ? self::STATUS : null;
            }

            if ($args[0] !== 'master' && !$this->masterValues[$args[0]]) {//if not master but master does not exists
                return null;
            }
        }

        $this->requiredValues[$field] = true;

        return (($value === '' || !$value) && $value !== '0') ? self::STATUS : null;
    }

    public function hasMinLength(string $field, string $value, array $args): ?string
    {
        $value = trim($value);

        $error = strlen($value) < (int) $args[0] ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function hasMaxLength(string $field, string $value, array $args): ?string
    {
        $value = trim($value);

        $error = strlen($value) > (int) $args[0] ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function hasExactLength(string $field, string $value, array $args): ?string
    {
        $value = trim($value);

        $error = strlen($value) !== (int) $args[0] ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isValidGsm(string $field, string $value): ?string
    {
        $value = preg_replace('/\s+/', '', $value);

        $result = preg_grep('/^((\+|00)32\s?|0)4(60|[789]\d)(\s?\d{2}){3}$/', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isValidPassword(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep(
            '/^\S*(?=\S{8,})(?=\S*[_\+\-\*:=\$&@\?])(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/',
            explode('\n', $value)
        );

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isValidEmail(string $field, string $value): ?string
    {
        $value = trim($value);

        $error = filter_var($value, FILTER_VALIDATE_EMAIL) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isOneOf(string $field, string $value, array $args): ?string
    {
        $value = trim($value);

        if (!empty($args)) {
            $str = $args[0];

            foreach ($args as $index => $arg) {
                if ($index > 0) {
                    $str .= '|' . $arg;
                }
            }

            $result = preg_grep(sprintf("/^(%s)\$/", $str), explode('\n', $value));

            $error = empty($result) ? self::STATUS : null;

            return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
        }

        return null;
    }

    public function isSameAs(string $field, string $value, array $args): ?string
    {
        $value = trim($value);
        $value1 = trim($this->values[$args[0]]);

        $error = ($value !== $value1) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isAllAlpha(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/[a-ząćęłńśóźżçâàèéëàùòôö\-_*]$/i', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isAllAlphaNumeric(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/[a-z0-9ąćęłńśóźżçâàèéëùòôö\-_*]$/i', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isText(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/[a-z0-9ąćęłńśóźżçâàèéëàùòôö\-._*:,();]$/i', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isOpenSslKey(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/[a-zA-Z0-9\/\-+=_*]$/i', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isAllNumeric(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/^\d+$/', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isInteger(string $field, string $value): ?string
    {
        $error = ctype_digit($value) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && ($value === null || empty($value))) ? null : $error;
    }

    public function isIdsAlfaNumericString(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/^[0-9a-z\,]*$/', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isIdsAlfaString(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/^[a-z\,]*$/', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isIdsNumericString(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/^[0-9\,]*$/', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isFloat(string $field, string $value): ?string
    {
        $value = trim($value);
        $value = preg_replace('/\s+/', self::STATUS, $value); // remove whitespaces
        $value = str_replace(',', '.', $value);// comas into dots

        if (!ctype_digit(str_replace('.','', $value))) {
            return self::STATUS;
        }

        $error = is_float((float) $value) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isValidTimestamp(String $field, String $value, array $args): ?string
    {
        $value = trim($value);
        $format = trim($args[0]); // 'Y-m-d H:i:s'

        $dateTime = DateTimeImmutable::createFromFormat($format, $value);

        $error = ($dateTime && $dateTime->format($format) === $value) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isValidTimeFormat(String $field, String $value): ?string
    {
        $value = trim($value);//20:59

        $result = preg_grep('/^(?:\d|[01]\d|2[0-3]):[0-5]\d$/', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isJsonEncoded(String $field, String $value): ?string
    {
        $value = trim($value);

        $result = json_decode($value, true, 512);

        $error = is_array($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isImageFilePath(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/^\S+\.(gif|jpg|jpeg|png)$/', explode('\n', $value));

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isUrl(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep(
            '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?$/',
            explode('\n', $value)
        );

        $error = empty($result) ? self::STATUS : null;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isNotHtml(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_grep('/<[^>]*>/', explode('\n', $value));

        $error = empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isPolishZip(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_match('/^(\d{2})(-\d{3})?$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isPolishNip(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_match('/^((\d{3}[- ]\d{3}[- ]\d{2}[- ]\d{2})|(\d{3}[- ]\d{2}[- ]\d{2}[- ]\d{3}))$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isPolishPhone(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_match('/^[0-9+]{8,13}$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isDateTimeValue(string $field, string $value): ?string
    {
        $value = trim($value);

        $result = preg_match('/^(\d{4})-([0-1]\d)-([0-3]\d)\s([0-1]\d|[2][0-3]):([0-5]\d):([0-5]\d)$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isGsmValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/\s+/', '', $value);

        $result = preg_match('/^\+(\d{2})(\d{3})(\d{3})(\d{3})(\d{0,10})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isPhoneValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/\s+/', '', $value );

        $result = preg_match('/^\+(\d{2})(\d{2})(\d{3})(\d{2})(\d{2})(\d{1,3})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isNumberValue(string $field, string $value): ?string
    {
        $value = preg_replace('/\s+/', '', $value);

        $error = filter_var($value, FILTER_VALIDATE_FLOAT) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isMoneyValue(string $field, string $value, array $args): ?string
    {
        return $this->isNumberSuffixValue($field, $value, $args);
    }

    public function isRateValue(string $field, string $value, array $args): ?string
    {
        return $this->isNumberSuffixValue($field, $value, $args);
    }

    protected function isNumberSuffixValue(string $field, string $value, array $args): ?string
    {
        $suffix = $args[0];

        $value = preg_replace( '/\s+/', '', $value );

        if (strpos($value, $suffix) !== false) {
            return $this->isNumberValue(
                $field,
                rtrim($value, $suffix)
            );
        }

        $error = self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isTvaValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/[\W]/', '', $value );

        $result = preg_match('/^([A-Z]{2})([A-Z0-9]{6,15})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isTvaPlValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/[\W]/', '', $value );

        $result = preg_match('/^PL(\d{3})(\d{3})(\d{2})(\d{2})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isNipValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/\s+/', '', $value );

        $result = preg_match('/^(\d{3})(\d{3})(\d{2})(\d{2})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isZipValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/\s+/', '', $value );

        $result = preg_match('/^([A-Z\d]{4-10})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isZipPlValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/\s+/', '', $value );

        $result = preg_match('/^(\d{2})-(\d{3})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }

    public function isBankAccountValue(string $field, string $value): ?string
    {
        $value = preg_replace( '/\s+/', '', $value );

        $result = preg_match('/^([A-Z]{2})([A-Z0-9]{6,15})$/', $value);

        $error = !empty($result) ? null : self::STATUS;

        return (!isset($this->requiredValues[$field]) && empty($value)) ? null : $error;
    }
}
