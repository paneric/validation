<?php

declare(strict_types=1);

namespace Paneric\Validation\Service;

use function is_array;

class MessageService
{
    public function setMessages(array $config, array $values, array $errors, array $alertsCluster, string $local): array
    {
        $alerts = $this->mutateAlertsCluster($alertsCluster, $local);

        $messages = [];

        foreach ($config as $field => $cfg) {
            $fieldValue = $values[$field] ?? '';

            if (!is_array($fieldValue)) {
                $rules = $errors[$field];

                foreach ($rules as $rule => $parameters) {
                    if ($parameters['error'] === RuleService::STATUS) {
                        $alert = $alerts[$rule];

                        $arguments = $parameters['arguments'];

                        $messages[$field][$rule] = $this->prepare($alert, $arguments);
                    }
                }
            }

            if (is_array($fieldValue)) {
                foreach ($fieldValue as $key => $value) {
                    $rules = $errors[$field][$key];

                    foreach ($rules as $rule => $parameters) {
                        if ($parameters['error'] === RuleService::STATUS) {
                            $alert = $alerts[$rule];

                            $arguments = $parameters['arguments'];

                            $messages[$field][$key][$rule] = $this->prepare($alert, $arguments);
                        }
                    }
                }
            }
        }

        return $messages;
    }

    private function mutateAlertsCluster(array $alertsCluster, string $local): array
    {
        $alerts = [];

        foreach ($alertsCluster as $row) {
            $rule = $row['rule'];

            $alerts[$rule] = $row[$local];
        }

        return $alerts;
    }

    private function prepare(string $alert, array $arguments): string
    {
        $size = sizeof($arguments);

        if ($size > 0 && strpos($alert, '%s') !== false) {
            $needle = '%s';

            for ($i = 1; $i < $size; $i++) {

                $needle .= ', %s';
            }

            $alert = str_replace('%s', $needle, $alert);

            return vsprintf($alert, $arguments);
        }

        return $alert;
    }
}
