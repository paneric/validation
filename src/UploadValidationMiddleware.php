<?php

declare(strict_types=1);

namespace Paneric\Validation;

use Paneric\Validation\Traits\ValidationTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\UploadedFileInterface;

class UploadValidationMiddleware implements MiddlewareInterface
{
    use ValidationTrait;

    private $errors = [
        'UPLOAD_ERR_OK',
        'UPLOAD_ERR_INI_SIZE',
        'UPLOAD_ERR_FORM_SIZE',
        'UPLOAD_ERR_PARTIAL',
        'UPLOAD_ERR_NO_FILE',
        'WTF',
        'UPLOAD_ERR_NO_TMP_DIR',
        'UPLOAD_ERR_CANT_WRITE',
        'UPLOAD_ERR_EXTENSION',
    ];

    private $validator;
    private $config;

    public function __construct(
        Validator $validator,
        array $configs
    ) {
        $this->validator = $validator; // used by trait
        $this->config = $configs;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeName = $request->getAttribute('route_name');

        $this->config = $this->config[$routeName];

        if (!in_array($request->getMethod(), $this->config['methods'], true)) {
            $request = $request->withAttribute('upload_validation', []);

            return $handler->handle($request);
        }

        unset($this->config['methods']);

        $files = $request->getUploadedFiles();

        $report = $this->validate($files);

        return $this->setValidationReport(
            $request,
            $handler,
            [UploadedFileInterface::class => $report]
        );
    }

    private function validate(array $files): array
    {
        $report = [];

        foreach ($this->config as $fieldName => $rules) {
            $field = $files[$fieldName];



            $required = array_key_exists('required', $rules);

            if (is_array($field)) {
                if ($required && $field[0]->getError() === 4) {
                    $report[$fieldName]['required'] = 'this_field_is_required';
                }
            }

            if (!is_array($field)) {
                if ($required && $field->getError() === 4) {
                    $report[$fieldName]['required'] = 'this_field_is_required';
                }
            }



            $acceptedMediaTypes = $rules['media_type'];

            if (is_array($field)) {

                foreach ($field as $i => $input) {
                    if ($input->getError() === 0) {
                        if(!in_array($input->getClientMediaType(), $acceptedMediaTypes, true)){
                            $report[$fieldName][$i]['media_type'] = 'invalid_media_type';
                        }

                        $errorNumber = $input->getError();

                        if($errorNumber > 0) {
                            $report[$fieldName][$i]['error'] = $this->errors[$errorNumber];
                        }
                    }
                }

                $countRange = $rules['count_range'];

                $countFiles = count($field);

                if ($countRange[0] > $countFiles || $countFiles > $countRange[1]) {
                    $report[$fieldName]['count_range'] = 'incorrect_files_number';
                }
            }

            if (!is_array($field)) {
                if ($field->getError() === 0) {
                    if(!in_array($field->getClientMediaType(), $acceptedMediaTypes, true)){
                        $report[$fieldName]['media_type'] = 'invalid_media_type';
                    }

                    $errorNumber = $field->getError();

                    if($errorNumber > 0) {
                        $report[$fieldName]['error'] = $this->errors[$errorNumber];
                    }
                }
            }
        }

        return $report;
    }
}

/* config example :

return => [
    'upload_validation' => [

        'article.files.upload' => [
            'methods' => ['POST'],
            'files' => [
                'required' => [],
                'count_range' => [3, 12],
                'media_type' => ['image/jpg', 'image/jpeg', 'image/png']
            ],
            'files' => [
                'required' => [],
                'media_type' => ['image/jpg', 'image/jpeg', 'image/png']
            ],
        ],
    ],
];
*/
