<?php

declare(strict_types=1);

namespace Paneric\Validation;

use Paneric\Validation\Traits\ValidationTrait;
use Psr\Http\Message\ServerRequestInterface;

class ValidationService
{
    use ValidationTrait;

    public const ROUTE = '__route__';

    private $validator;
    private $configs;

    public function __construct(
        Validator $validator,
        array $configs
    ) {
        $this->validator = $validator;
        $this->configs = $configs;
    }

    public function getReport(ServerRequestInterface $request): array
    {
        $routeName = $request->getAttribute('route_name');

        $this->configs = $this->configs[$routeName];

        if (!in_array($request->getMethod(), $this->configs['methods'], true)) {
            return [];
        }

        $report = $this->generateReport($request);

        $this->setServiceReport($report);

        return $report;
    }
}
