<?php

return [
    'route.name' => [
        TestDTO::class => [
            'request_attribute' => 'authentication',
            'rules' => [
                'field_name' => [
                    'required' => [],
                    'has_min_length' => [4],
                    'has_max_length' => [4],
                    'has_exact_length' => [4],
                    'is_valid_gsm' => [],
                    'is_valid_password' => [],
                    'is_valid_email' => [],
                    'is_one_of' => ['WIFI', 'MOBILE', 'ANY'],
                    'is_same_as' => ['password_hash'],
                    'is_all_alpha' => [],
                    'is_all_alpha_numeric' => [],
                    'is_all_numeric' => [],
                    'is_integer' => [],
                    'is_ids_string' => [],
                    'is_float' => [],
                    'is_valid_timestamp' => ['Y-m-d H:i:s'],
                    'is_valid_time_format' => [],
                    'is_image_file_path' => [],
                    'is_url' => [],
                    'is_not_html' => [],
                ],
            ],
        ],
    ],
];
