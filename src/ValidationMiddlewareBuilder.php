<?php

declare(strict_types=1);

namespace Paneric\Validation;

class ValidationMiddlewareBuilder
{
    public function build (string $local, array $validation, array $seoValidation = []): ValidationMiddleware
    {
        $validatorBuilder = new ValidatorBuilder();
        $validator = $validatorBuilder->build($local);

        if (!empty($seoValidation)) {
            $validation = array_merge($validation, $seoValidation);
        }

        return new ValidationMiddleware(
            new ValidationService($validator, $validation)
        );
    }
}
