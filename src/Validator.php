<?php

declare(strict_types=1);

namespace Paneric\Validation;

use Paneric\Interfaces\Validator\ValidatorInterface;
use Paneric\Validation\Service\ErrorService;
use Paneric\Validation\Service\MessageService;

class Validator implements ValidatorInterface
{
    private $errorService;
    private $messageService;

    private $alertsCluster;
    private $local;

    private $report;

    public function __construct (
        ErrorService $errorService,
        MessageService $messageService,
        array $alertsCluster,
        string $local
    ) {
        $this->errorService = $errorService;
        $this->messageService = $messageService;

        $this->alertsCluster = $alertsCluster;
        $this->local = $local;
    }

    public function setMessages(array $rules, array $values, array $requestAttribute = []): array
    {
        return $this->messageService->setMessages(
            $rules,
            $values,
            $this->errorService->setErrors($rules, $values, $requestAttribute),
            $this->alertsCluster,
            $this->local
        );
    }

    public function setReport(array $report): void
    {
        $this->report = $report;
    }

    public function getReport(): ?array
    {
        return $this->report;
    }
}
