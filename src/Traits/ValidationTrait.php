<?php

declare(strict_types=1);

namespace Paneric\Validation\Traits;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;

trait ValidationTrait
{
    protected function generateReport(ServerRequestInterface $request): array
    {
        unset($this->configs['methods']);

        $report = [];

        $requestAttributes = $this->getRequestAttributes($request);

        foreach ($this->configs as $dtoName => $config) {
            $requestAttributeValues = [];

            if (isset($config['request_attribute'])) {
                $values = $request->getAttribute($config['request_attribute']);
                if ($values !== null && is_array($values)) {
                    $requestAttributeValues = $values;
                }
            }

            $fields = array_keys($config['rules']);

            $purgedRequestAttributes = $this->purgeRequestAttributes($requestAttributes, $fields);

            if (!empty($purgedRequestAttributes)) { // Multiple Form
                $attributesConverted = $this->convertMultipleFormAttributes(
                    $purgedRequestAttributes,
                    $dtoName,
                    $fields
                );
            }

            if (empty($purgedRequestAttributes)) { // Standard Form
                $hydrator = new $dtoName();

                $hydrator->hydrate($requestAttributes);

                $attributesConverted = $hydrator->convert();
            }

            $report[$dtoName] = $this->validator->setMessages(
                $config['rules'],
                $attributesConverted,
                $requestAttributeValues
            );
        }

        return $report;
    }

    protected function getRequestAttributes(ServerRequestInterface $request): array
    {
        $requestMethod = $request->getMethod();

        if (in_array($requestMethod, ['POST', 'PUT', 'PATCH'])) {
            return $request->getParsedBody();
        }

        if (in_array($requestMethod, ['GET', 'DELETE'])) {
            parse_str($request->getUri()->getQuery(), $requestAttributes);

            return $requestAttributes;
        }

        throw new InvalidArgumentException(
            'PANERIC: Request method ' . $requestMethod . ' not allowed !!!'
        );
    }

    protected function purgeRequestAttributes(array $requestAttributes, array $fields): array
    {
        $purgedAttributes = [];

        foreach ($requestAttributes as $key => $attributes) {
            if (in_array($key, $fields) && is_array($attributes)) {
                $purgedAttributes[$key] = $attributes;
            }
        }

        return $purgedAttributes;
    }

    protected function convertMultipleFormAttributes(array $purgedAttributes, String $dtoName, array $fields): array
    {
        $firstField = array_key_first($purgedAttributes);

        $indexes = array_keys($purgedAttributes[$firstField]);

        $convertedAttributes = [];

        foreach($indexes as $index) {
            $hydrationArray = [];
            $notEmpty = false;

            foreach ($purgedAttributes as $key => $attributes) {
                if (!empty($purgedAttributes[$key][$index])) {
                    $hydrationArray[$key] = $purgedAttributes[$key][$index];
                }
            }

            if (!empty($hydrationArray)) {
                $hydrator = new $dtoName();

                $hydrator->hydrate($hydrationArray);
                $filteredAttributes = $hydrator->convert();

                foreach ($fields as $key) {
                    if (isset($filteredAttributes[$key])) {
                        $convertedAttributes[$key][$index] = $filteredAttributes[$key];
                    } else {
                        $convertedAttributes[$key][$index] = '';
                    }
                }
            }
        }

        return $convertedAttributes;
    }

    protected function setServiceReport(array $report): void
    {
        $validatorReport = $this->validator->getReport();

        if ($validatorReport === null) {
            $this->validator->setReport($report);

            return;
        }

        $validatorReport = array_merge($validatorReport, $report);

        $this->validator->setReport($validatorReport);
    }
}
