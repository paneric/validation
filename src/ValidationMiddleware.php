<?php

declare(strict_types=1);

namespace Paneric\Validation;

use Paneric\Validation\Traits\ValidationTrait;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class ValidationMiddleware  implements MiddlewareInterface
{
    use ValidationTrait;

    protected $validationService;

    public function __construct(
        ValidationService $validationService
    ) {
        $this->validationService = $validationService;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $request = $request->withAttribute(
            'validation',
            $this->validationService->getReport($request)
        );

        return $handler->handle($request);
    }
}
