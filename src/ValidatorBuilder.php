<?php

declare(strict_types=1);

namespace Paneric\Validation;

use Paneric\Validation\Service\ErrorService;
use Paneric\Validation\Service\MessageService;
use Paneric\Validation\Service\RuleService;

class ValidatorBuilder
{
    public function build(string $local): Validator
    {
        return new Validator(
            new ErrorService(new RuleService()),
            new MessageService(),
            (array) require ROOT_FOLDER . 'vendor/paneric/validation/src/alerts.php',
            $local
        );
    }
}
