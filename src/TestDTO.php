<?php

namespace Paneric\Validation;

use Paneric\Interfaces\Hydrator\HydratorInterface;

class TestDTO implements HydratorInterface
{
    public function hydrate(array $args): self
    {
        return $this;
    }

    public function convert(): array
    {
        return [];
    }

    public function serialize(): array
    {
        // TODO: Implement serialize() method.
    }

    public function unserialize()
    {
        // TODO: Implement unserialize() method.
    }
}
